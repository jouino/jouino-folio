//action for meteor on client side
if (Meteor.isClient) {
  Tracker.autorun(function (computation) {

  });

  Template.projects_template.helpers({
    Projects: function () {
      return Projects_collection.find().fetch();
    }
  });

  Template.project_single.helpers({
    name: function() {
      return Session.get("project_title");
    },
    year: function() {
      return Session.get("project_year");
    },
    job: function() {
      return Session.get("project_job");
    },
    work_type: function() {
      return Session.get("project_work_type");
    },
    location: function() {
      return Session.get("project_location");
    },
    main_content: function() {
      return Session.get("project_main_content");
    }
  });

  Template.homepage_template.events({
    'mouseenter .box': function (event) {
       $(event.target).addClass('active');
    },
    'mouseleave .box': function (event) {
       $(event.target).removeClass('active');
    }
  });

  Template.projects_template.events({
    'mouseenter .projects_list li a': function (event) {
       $(event.target).addClass('active');
       $('.background_projects .logo_'+$(event.target).data('idproject')).addClass('active');
       $('.projects_list li a').addClass('inactive');
    },
    'mouseleave .projects_list li a': function (event) {
       $(event.target).removeClass('active');
       $('.background_projects .svg_logo_wrapper').removeClass('active');
       $('.projects_list li a').removeClass('inactive');
    }
  });

  Template.about_template.events({
    'click #twitter': function(){
      window.open('http://www.twitter.com/jouino/', '_blank');
    },
    'click #linkedin': function(){
      window.open('https://www.linkedin.com/in/julienjounieau', '_blank');
    },
    'click #instagram_official': function(){
      window.open('https://instagram.com/jouino/', '_blank');
    }
  });

  Meteor.startup(function () {
    $( window ).load(function() {
      $('.et-wrapper').addClass('loaded');
    });

    PageTransitions.init();
  });
}

