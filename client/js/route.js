Meteor.subscribe("all-projects");

//default config - need to debug
Router.configure({
  // template: 'ApplicationLayout',
  // layout: 'ApplicationLayout',
  notFoundTemplate: 'notFound',
  loadingTemplate: 'loading',
});

//projects list router
Router.route('/about/', {
  after: function () {
    document.title = "About";
  },
  action: function () {
    if ($('#about_wrapper').hasClass('notvisible')) {
      $('#about_wrapper').removeClass('notvisible');
      init_about_svg();
    };

  },
});

//projects list router
Router.route('/projects/', {
  after: function () {
    document.title = "My projects";
  },
  action: function () {

  },
});

//single project page router
Router.route('/project/:_id', {
  onBeforeAction: function () {
    //check if first load - if not animation to hide previous content and display new one
    router_wrapper = this;
    $('#project_single_wrapper').removeClass('change-in');
    if (this.params._id) {
      if (!$('#project_single_wrapper h1 span').html()) {
        this.next();
      } else {
        $('#project_single_wrapper').addClass('change-out');
        $('#project_single_wrapper.change-out h1').bind("transitionend animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", function(){
          Session.set("project_title", Projects_collection.findOne({url: router_wrapper.params._id}).name);
          Session.set("project_year", Projects_collection.findOne({url: router_wrapper.params._id}).year);
          Session.set("project_job", Projects_collection.findOne({url: router_wrapper.params._id}).job);
          Session.set("project_work_type", Projects_collection.findOne({url: router_wrapper.params._id}).work_type);
          Session.set("project_location", Projects_collection.findOne({url: router_wrapper.params._id}).location);
          Session.set("project_main_content", Projects_collection.findOne({url: router_wrapper.params._id}).main_content);
          document.title = Projects_collection.findOne({url: router_wrapper.params._id}).name;

          // router_wrapper.next();
          $('#project_single_wrapper').removeClass('change-out');
          $('#project_single_wrapper').addClass('change-in');
          $('#project_single_wrapper.change-in h1').bind("transitionend animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", function(){
            $('#project_single_wrapper').removeClass('change-in');
          });
        });
      }
    }
  },
  action: function () {
    if (this.params._id&&Projects_collection) {
      Session.set("project_title", Projects_collection.findOne({url: this.params._id}).name);
      Session.set("project_year", Projects_collection.findOne({url: this.params._id}).year);
      Session.set("project_job", Projects_collection.findOne({url: this.params._id}).job);
      Session.set("project_work_type", Projects_collection.findOne({url: this.params._id}).work_type);
      Session.set("project_location", Projects_collection.findOne({url: this.params._id}).location);
      Session.set("project_main_content", Projects_collection.findOne({url: this.params._id}).main_content);
      document.title = Projects_collection.findOne({url: this.params._id}).name;
    }
  },
});

//homepage route
Router.route('/', {
  after: function () {
    document.title = "Julien Jounieau | Freelance Front-end Developer";
  },
  onAfterAction: function () {

  },
  action: function () {
    // console.log('ok');
  },
});
