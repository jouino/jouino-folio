//change delay and offset for each svg path
init_about_svg = function init_about_svg() {
  if ($('.svg_wrapper_about')[0]) {
    $('.svg_wrapper_about svg #about path').each(function(index, element){
      $(this).css({
        'animation-delay':(index+1)*0.05+'s',
        'animation-duration':'0.05s'
      });
    });
    $('#twitter path, #linkedin path, #instagram_official path')
    .css({
      'animation-delay':($( ".svg_wrapper_about svg #about path" ).length*0.05)+0.5+'s',
      'animation-duration':'4s'
    });
    $('.svg_wrapper_about').addClass('active_hover');
  };
}
