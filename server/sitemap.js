sitemaps.add('/sitemap.xml', function() {
  return [{
    page: '/',
    changefreq: 'weekly'
  }, {
    page: '/about',
    changefreq: 'weekly'
  }, {
    page: '/projects',
    changefreq: 'weekly'
  }];
});
